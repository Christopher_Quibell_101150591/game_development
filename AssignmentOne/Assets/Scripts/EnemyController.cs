﻿/*Author: Christopher Quibell 101150591
 * Version 1.0
 * Creation Date: Oct 5
 * Revised Date: Oct 13
 * This class is the controller used to control the enemy.  The enemy speed is made here and the positions are assigned here
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
	//Below are all the variables used throughout the program
	[SerializeField]
	float minYSpeed = 5f;
	[SerializeField]
	float maxYSpeed = 10f;
	[SerializeField]
	float minXSpeed = -2f;
	[SerializeField]
	float maxXSpeed = 2f;

	[SerializeField]
	float upperY = 0f;
	[SerializeField]
	float lowerY = 0f;
	[SerializeField]
	float endX = 0f;

	private Transform _transform;
	private Vector2 _currentSpeed;
	private Vector2 _currentPosition;
	private MathFunctions mathFunctions = new MathFunctions();

	// Use this for initialization
	void Start () {
		//Set the Transformation variable when the program starts
		_transform = gameObject.GetComponent<Transform> ();
		Reset ();
	}

	//This sets the enemy back to its starting position and changes the speed and position on the Y axis.
	public void Reset(){
		_currentSpeed = mathFunctions.CalculateSpeed(maxXSpeed,minXSpeed,maxYSpeed,minYSpeed);
		_transform.position = mathFunctions.CalculatePosition (lowerY, upperY);
	}

	// Update is called once per frame
	void Update () {
		//Change the enemies position
		_currentPosition = _transform.position;
		_currentPosition -= _currentSpeed;
		_transform.position = _currentPosition;
		//If the enemies position is at the end of the camera or below or above the desginated positions then the 
		//Enemy is reset
		if (_currentPosition.x <= endX || _currentPosition.y <= lowerY || _currentPosition.y >= upperY) {
			Reset ();
		}
	}
}
