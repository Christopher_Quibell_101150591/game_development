﻿/*Author: Christopher Quibell 101150591
 * Version 1.0
 * Creation Date: Oct 5
 * Revised Date: Oct 17
 * This class controls the main game such as the winning score, health points, and the UI that appears when the 
 * player either wins the game or loses the game.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
	//All variables needed in this class in order to function
	[SerializeField]
	Text lifeLabel;
	[SerializeField]
	Text scoreLabel;
	[SerializeField]
	Text statusLabel;
	[SerializeField]
	Text finalScore;
	[SerializeField]
	Button resetBtn;
	//The score, winning score and life variables
	private int _score = 0;
	[SerializeField] private int _winningScore = 900;
	[SerializeField] private int _life = 3;

	public CoinController coin;
	public EnemyController enemy;
	public HeroController hero;

	//The score method that has getters and setters that sets the text for the ui and the score value.
	public int Score{
		get{ return _score; }
		set{ 
			_score = value;

			if (_score >= _winningScore)
				//Winner Of Game ui
				winner ();
			else
				scoreLabel.text = "Score: " + _score;
		}

	}
	//The life method that has getters and setters that sets the text for the ui and the life value.
	public int Life{
		get{ return _life; }
		set{ 
			_life = value;


			if (_life <= 0) 
				//game over
				gameOver();
			else
				//Add the new life value
				lifeLabel.text = "Life: " + _life;
		}
	}

	//Called when the game starts.
	private void initialize(){
		//Set the score to 0 and the life to whatever has been set in the interface
		Score = 0;
		Life = _life;
		//Set the menu labels to false so they are not seen
		statusLabel.gameObject.SetActive (false);
		resetBtn.gameObject.SetActive (false);
		finalScore.gameObject.SetActive (false);
		//Set the ui during gameplay to true
		lifeLabel.gameObject.SetActive (true);
		scoreLabel.gameObject.SetActive (true);
	}
	//If the player runs out of life then show game over and the menu
	private void gameOver(){
		statusLabel.text = "Game Over!";
		DisplayResults ();
	}
	//If the score equals what the winning score is display winner and the menu
	private void winner(){
		statusLabel.text = "Winner!!!!";
		DisplayResults ();
	}

	// Use this for initialization
	void Start () {
		initialize ();
	}

	// Update is called once per frame
	void Update () {

	}
	//Reset the game when the button has been clicked
	public void ResetBtnClick(){

		SceneManager.
		LoadScene (
			SceneManager.GetActiveScene ().name);

	}
	//Display the results of the game, disable the in game ui and show the menu
	private void DisplayResults(){
		finalScore.text = "Final Score: " + _score;
		statusLabel.gameObject.SetActive (true);
		resetBtn.gameObject.SetActive (true);
		finalScore.gameObject.SetActive (true);

		lifeLabel.gameObject.SetActive (false);
		scoreLabel.gameObject.SetActive (false);

		coin.gameObject.SetActive (false);
		enemy.gameObject.SetActive (false);
		hero.gameObject.SetActive (false);
	}

}
