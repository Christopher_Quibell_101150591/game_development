﻿/*Author: Christopher Quibell 101150591
 * Version 1.0
 * Creation Date: Oct 5
 * Revised Date: Oct 13
 * This class controls the movement of the background.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour {

	//Public variables
	[SerializeField] private float speed = 5f;
	[SerializeField] private float startX = 0F;
	[SerializeField] private float endX = 0F;


	//private variables
	private Transform _transform;
	private Vector2 _currentPos;

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;
		Reset();

	}

	// Update is called once per frame
	void Update () {
		_currentPos =  _transform.position;
		_currentPos -= new Vector2(speed,0);

		if(_currentPos.x < endX){
			Reset();
		}

		_transform.position = _currentPos;
	}
	//Reset the background to the starting x posisiton.
	private void Reset(){
		_currentPos = new Vector2(startX,0);
	}
}
