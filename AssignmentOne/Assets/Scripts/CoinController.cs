﻿/*Author: Christopher Quibell 101150591
 * Version 1.0
 * Creation Date: Oct 5
 * Revised Date: Oct 17
 * This class controls the coin used to collect points
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour {
	//The variables used throughout the class
	[SerializeField] private float upperY = 0F;
	[SerializeField] private float lowerY = 0F;
	[SerializeField] private float endX = 0F;

	[SerializeField]
	float minYSpeed = 5f;
	[SerializeField]
	float maxYSpeed = 10f;
	[SerializeField]
	float minXSpeed = -2f;
	[SerializeField]
	float maxXSpeed = 2f;

	private Transform _transform;
	private Vector2 _currentSpeed;
	private Vector2 _currentPosition;
	private MathFunctions mathFunctions = new MathFunctions();
	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		Reset ();
	}
	
	// Update is called once per frame
	public void Reset(){
		//Update the speed based on a random number
		_currentSpeed = mathFunctions.CalculateSpeed(maxXSpeed,minXSpeed,maxYSpeed,minYSpeed);
		//Calculate the position on the y axis
		_transform.position = mathFunctions.CalculatePosition (upperY, lowerY);
	}

	// Update is called once per frame
	void Update () {
		_currentPosition = _transform.position;
		_currentPosition -= _currentSpeed;
		_transform.position = _currentPosition;
		//Make sure the coin is between the upper and lowe bounds on the y axis and not past the farther x bound
		if (_currentPosition.x <= endX || _currentPosition.y <= lowerY || _currentPosition.y >= upperY) {
			Reset ();
		}
	}
}

