﻿/*Author: Christopher Quibell 101150591
 * Version 1.0
 * Creation Date: Oct 5
 * Revised Date: Oct 17
 * This class controls basic math throughout the game.  The class controls the random speed and location of the objects calling it(coin,enemy)
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathFunctions {

	//Basic constructor
	public MathFunctions(){
		
	}
	//This figures out the random x and y speed to use
	public Vector2 CalculateSpeed(float maxX, float minX,float maxY, float minY){
		float xSpeed = Random.Range (minX, maxX);
		float ySpeed = Random.Range (minY, maxY);
		//Returns the new speed that the object is moving at
		return new Vector2 (xSpeed, ySpeed);
	}
	public Vector2 CalculatePosition(float upperY, float lowerY){
		//Random number in the y region
		float y = Random.Range (lowerY, upperY); 
		//Returns a random number and a random number in the x column minus 25 as the enemy will be falling
		return new Vector2 (-25+Random.Range(0,100),y);
	}
}
