﻿/*Author: Christopher Quibell 101150591
 * Version 1.0
 * Creation Date: Oct 5
 * Revised Date: Oct 13
 * This class controls the heros movement and the speed the hero moves at
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HeroController : MonoBehaviour {


	//Public variables
	[SerializeField] private float speed = 7f;
	[SerializeField] private float UpperY = 0F;
	[SerializeField] private float LowerY = 0F;
	//[SerializeField] private int health;


	//private variables
	private Transform _transform;
	private Vector2 _currentPos;
	//private int score = 0;

	//public Text scoreText;
	//public Text healthText;

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform> ();
		_currentPos = _transform.position;
	}

	// Update is called once per frame
	void Update () {
		//Move hero up using the W key.
		if(Input.GetKey(KeyCode.W)){
			_currentPos += new Vector2(0,speed);
		}
		//Move hero down using the D key
		if(Input.GetKey(KeyCode.S)){
			_currentPos -= new Vector2(0,speed);
		}
		//Make sure the hero is within the proper bounds
		CheckBounds();
		_transform.position = _currentPos;
	}
	private void CheckBounds(){
		//Check if the heros bounds are within view of the camera
		if(_currentPos.y < LowerY){
			_currentPos.y = LowerY;
		}
		if(_currentPos.y > UpperY){
			_currentPos.y = UpperY;
		}
	}
}
