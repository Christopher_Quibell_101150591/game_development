﻿/*Author: Christopher Quibell 101150591
 * Version 1.1
 * Creation Date: Oct 5
 * Revised Date: Oct 17
 * This class controls the collision behaviour between the plan and the point system and the enemies.  It toggles the sounds and the points and health
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroCollision : MonoBehaviour {
	//Below are all the variables used in the class.  
	[SerializeField]
	GameController gameController;

	[SerializeField]
	GameObject explosion;

	[SerializeField]
	private AudioSource bangSource;
	[SerializeField]
	private AudioSource boopSource;

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {

	}
	/*This method is what is trigggered when the hero collides with either the plane or the enemy
	 * The method checks what is colliding with the plane and either adds points, and plays a boop or
	 * removes health and plays a bang.
	*/
	public void OnTriggerEnter2D(Collider2D other){


		if (other.gameObject.tag.Equals ("coin")) {
			//Add points
			gameController.Score+=100;
			//Make sure a audio file is assigned to the variable
			if (boopSource != null)
				boopSource.Play ();
			//Reset the coin if plane collides with it
			other.gameObject.
			GetComponent<CoinController> ().Reset ();
		}else if(other.gameObject.tag.Equals ("enemy")){
			//Make sure the audio source is assigned to the bang audio source
			if(bangSource != null)
				bangSource.Play ();
			//Play the explosion animation
			Instantiate (explosion)
				.GetComponent<Transform> ()
				.position = other.gameObject
					.GetComponent<Transform> ()
					.position;
			//Reset the enemy to the start position
			other.gameObject.
			GetComponent<EnemyController> ().Reset ();
			//Remove life
			gameController.Life-=1;
		}

	}

}

